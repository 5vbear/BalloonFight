﻿using System;
using System.Collections.Generic;
using Assets.coreScripts.common;
using Assets.coreScripts.fighting.model;
using Assets.coreScripts.fighting.util;
using strange.extensions.dispatcher.eventdispatcher.api;

namespace Assets.coreScripts.fighting.view
{
    public class PropsMediator : FightingBaseEventMediator
    {
        [Inject]
        public PropsView view { get; set; }
        [Inject]
        public IUnitAPI customAPI { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
        }
        public override void OnRemove()
        {
            UpdateListeners(false);
        }


        protected override void UpdateListeners(bool enable)
        {
            view.dispatcher.UpdateListener(enable, GameConfig.PropsState.PROPS_COMMAND, Command);
            view.dispatcher.UpdateListener(enable, GameConfig.PropsState.STARTGAME, Command);


            dispatcher.UpdateListener(enable, GameConfig.PropsState.WIN,Win);
            dispatcher.UpdateListener(enable, GameConfig.PropsState.LOSE, Lose);
            base.UpdateListeners(enable);
        }


        protected override void OnGameRestart()
        {
            view.gameObject.SetActive(false);
            base.OnGameRestart();
        }

        private void Lose(IEvent payload)
        {
            view.titleImg.sprite = customAPI.LoadSprite("lose");
            view.titleImg.SetNativeSize();
            view.gameObject.SetActive(true);
        }

        private void Win(IEvent payload)
        {
            view.titleImg.sprite = customAPI.LoadSprite("win");
            view.titleImg.SetNativeSize();
            view.gameObject.SetActive(true);
        }

        private void Command(IEvent e)
        {
            e.type = GameConfig.PropsState.PROPS_COMMAND;
            dispatcher.Dispatch(e);
        }

        //private void Show() { gameObject.SetActive(true); }


        //private void Pause(IEvent e)
        //{
        //    CustomPropsEventData data = e as CustomPropsEventData;
        //    data.type = GameConfig.PropsState.PAUSE;
        //    dispatcher.Dispatch(GameConfig.PropsState.PAUSE, data);
        //}

    }
}
