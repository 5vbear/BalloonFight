﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Assets.coreScripts.main.model;
//using UnityEngine;

//namespace Assets.coreScripts.fighting.view
//{
//    public class ReelBackgroundView : FightingBaseView
//    {
//        private float distance;
//        internal StageConfig config;
//        private GameObject tempPrefab { get; set; }
//        private GameObject tempSourceObj { get; set; }

//        public Transform targetTransform;


//        public void Init(StageConfig cfg)
//        {
//            config = cfg;

//            for (int i = 0; i < cfg.ReelList.Count; i++)
//            {
//                if (tempSourceObj != null && cfg.ReelList[i].BackgroundName == tempSourceObj.gameObject.name)
//                {
//                    //Jump Out
//                }
//                else { tempSourceObj = Resources.Load<GameObject>(string.Format("Prefab/CoreFighting/StageElement/{0}", cfg.ReelList[i])); }


//                tempPrefab = GameObject.Instantiate(tempSourceObj) as GameObject;
//                tempPrefab.transform.parent = targetTransform;
//                tempPrefab.SetActive(true);
//                tempPrefab.transform.localScale = Vector3.one;
//                tempPrefab.transform.localPosition = new Vector3(GameConfig.BackgroundImageWidth * i, 0, 0);
//            }

//            GameConfig.REEL_STATE = true;
//            rigidbody2D.velocity = new Vector2(GameConfig.REEL_SPEED, 0);
//            distance = (GameConfig.TestPage - 1) * GameConfig.BackgroundImageWidth;
//        }

//        public override void GameUpdate()
//        {
//            if (transform.localPosition.x >= distance)
//            {
//                GameConfig.REEL_STATE = false;
//                rigidbody2D.velocity = Vector2.zero;
//            }
//        }

//        public override void Init()
//        {
//            base.Init();
//        }

//    }
//}
