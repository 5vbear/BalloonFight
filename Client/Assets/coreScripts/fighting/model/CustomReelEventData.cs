﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using strange.extensions.dispatcher.eventdispatcher.api;

namespace Assets.coreScripts.fighting.model
{
    public class CustomReelEventData : CustomEventData
    {
        public float speed { get; set; }
    }
}
