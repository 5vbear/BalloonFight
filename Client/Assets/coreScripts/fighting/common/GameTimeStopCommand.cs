﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.coreScripts.fighting.util;
using strange.extensions.command.impl;

namespace Assets.coreScripts.fighting.common
{
   public  class GameTimeStopCommand:EventCommand
    {
       [Inject]
       public IGameTimer timer { get; set; }

       public override void Execute()
       {
           timer.Stop();
       }
    }
}
