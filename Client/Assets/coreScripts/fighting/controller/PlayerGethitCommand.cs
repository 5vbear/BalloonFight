﻿using System;
using System.Collections.Generic;
using strange.extensions.command.impl;
using strange.extensions.command.api;
using Assets.coreScripts.fighting.view;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.dispatcher.eventdispatcher.api;
using Assets.coreScripts.fighting.util;
using Assets.coreScripts.fighting.model;
using Assets.coreScripts.fighting.common;
using Assets.coreScripts.main.model;

namespace Assets.coreScripts.fighting.controller
{
    public class PlayerGethitCommand : EventCommand
    {
        [Inject]
        public FightingCommon common { get; set; }

        [Inject]
        public User user { get; set; }
        public override void Execute()
        {
            //if (evt == null)
            //{
            //    UnityEngine.Debug.Log("User HP -=1");
            //    return;
            //}

            CustomHitEventData data = evt as CustomHitEventData;

            switch (data.hitModel)
            {
                case GameConfig.ObstructionModel.OUTREEL_DIE:
                case GameConfig.ObstructionModel.DRAPSEA_DIE:
                    common.KillAllballoon(data.targetObj.transform);
                    CustomPropsEventData d1 = new CustomPropsEventData();
                    d1.type = GameConfig.PropsState.PROPS_COMMAND;
                    d1.propStatus = GameConfig.PropsState.LOSE;
                    dispatcher.Dispatch(GameConfig.PropsState.PROPS_COMMAND, d1);
                    //dispatcher.Dispatch(GameConfig.CoreEvent.GAME_OVER, GameConfig.PlayerState.LOUSER);
                    //common.KillAllballoon(data.currentObj.transform);
                    break;
                //case GameConfig.ObstructionModel.PLAYER_HURT:
                //    UnityEngine.Debug.Log("ObstructionModel.HURT");
                //    dispatcher.Dispatch(GameConfig.DisplayBoardEvent.UPDATE, new DisplayBoardEventData { hp = .1f, type = GameConfig.DisplayBoardEvent.UPDATE });
                //    break;
                case GameConfig.ObstructionModel.HURT_BOUNCE:
                    dispatcher.Dispatch(GameConfig.DisplayBoardEvent.UPDATE, new DisplayBoardEventData { hp = .1f, type = GameConfig.DisplayBoardEvent.UPDATE });
                    break;
                case GameConfig.ObstructionModel.PLAYER_BALLON_HURT:
                    if ((user.HP -= 1) <= 0)
                    {
                        CustomPropsEventData d = new CustomPropsEventData();
                        d.type = GameConfig.PropsState.PROPS_COMMAND;
                        d.propStatus = GameConfig.PropsState.LOSE;
                        dispatcher.Dispatch(GameConfig.PropsState.PROPS_COMMAND, d);
                    }
                    data.currentObj.SetActive(false);
                    break;
                default:
                    break;
            }

        }
    }
}
