﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.coreScripts.fighting.common;
using Assets.coreScripts.fighting.model;
using strange.extensions.command.impl;
using UnityEngine;

namespace Assets.coreScripts.fighting.controller
{
    public class ObstructionCommand : EventCommand
    {
        [Inject]
        public FightingCommon common { get; set; }
        public override void Execute()
        {
            CustomHitEventData data = evt as CustomHitEventData;

            Debug.Log(data.hitModel.ToString());
            switch (data.hitModel)
            {
                //case GameConfig.ObstructionModel.ON_HIT:
                //    break;
                case GameConfig.ObstructionModel.NORMAL:
                    //if (data.targetObj.rigidbody2D)
                    //{
                    //    data.targetObj.rigidbody2D.velocity = new Vector2(-GameConfig.REEL_SPEED,0);//new Vector2(Mathf.Clamp(data.targetObj.rigidbody2D.velocity.x - GameConfig.REEL_SPEED, GameConfig.REEL_SPEED, 10), data.targetObj.rigidbody2D.velocity.y);
                    //}
                    break;
                //case GameConfig.ObstructionModel.FINALLY:
                //    break;
                case GameConfig.ObstructionModel.OUTREEL_DIE:
                case GameConfig.ObstructionModel.DRAPSEA_DIE:
                case GameConfig.ObstructionModel.PLAYER_BALLON_HURT:
                    data.type = GameConfig.PlayerState.GET_HIT;
                    dispatcher.Dispatch(GameConfig.PlayerState.GET_HIT, data);
                    break;
                //case GameConfig.ObstructionModel.PLAYER_HURT:
                //    Debug.Log("Player Hurt ...");
                //    break;
                case GameConfig.ObstructionModel.ENEMY_BALLON_HURT:
                    data.type = GameConfig.EnemyState.GET_HIT;
                    dispatcher.Dispatch(GameConfig.EnemyState.GET_HIT, data);
                    break;
                case GameConfig.ObstructionModel.GOLD:
                    data.currentObj.SetActive(false);   
                    break;
                case GameConfig.ObstructionModel.BOUNCE:
                    common.AddDirctionForceToRigibody2D(data.targetObj, data.forceDir, data.force);
                    break;
                case GameConfig.ObstructionModel.HURT_BOUNCE:
                    common.AddDirctionForceToRigibody2D(data.targetObj, data.forceDir, data.force);
                    if (data.targetObj.tag.ToUpper() == "PLAYER")
                    {
                        data.type = GameConfig.PlayerState.GET_HIT;
                        dispatcher.Dispatch(GameConfig.PlayerState.GET_HIT, data);
                    }
                    if (data.targetObj.tag.ToUpper() == "ENEMY")
                    {
                        data.type = GameConfig.EnemyState.GET_HIT;
                        dispatcher.Dispatch(GameConfig.EnemyState.GET_HIT, data);
                    }

                    break;
                case GameConfig.ObstructionModel.ENTER_BOSSSTAGE:
                    dispatcher.Dispatch(
                        GameConfig.RoleEvent.SHOW_ENEMY,
                        new CustomAIEventData
                            {
                                type = GameConfig.RoleEvent.SHOW_ENEMY,
                                enemyLevel = GameConfig.EnemyLevel.BOSS
                            });
                    break;
                default:
                    break;
            }
        }
    }
}
