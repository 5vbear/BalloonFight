﻿using System;
using System.Collections.Generic;
using Assets.coreScripts.fighting.common;
using Assets.coreScripts.fighting.model;
using Assets.coreScripts.main.model;
using strange.extensions.command.impl;

namespace Assets.coreScripts.fighting.controller
{
    public class EnemyGetHitCommand:EventCommand
    {

        [Inject]
        public FightingCommon common { get; set; }
        [Inject]
        public User user { get; set; }

        public override void Execute()
        {
            //if (evt == null)
            //{
            //    UnityEngine.Debug.Log("Enemy HP -=1");
            //    return;
            //}

            CustomHitEventData data = evt as CustomHitEventData;

            switch (data.hitModel)
            {
                //case GameConfig.ObstructionModel.OUTREEL_DIE:
                //case GameConfig.ObstructionModel.DRAPSEA_DIE:
                //    dispatcher.Dispatch(GameConfig.CoreEvent.GAME_OVER, GameConfig.PlayerState.LOUSER);
                //    break;
                //case GameConfig.ObstructionModel.PLAYER_HURT:
                //    UnityEngine.Debug.Log("ObstructionModel.HURT");
                //    break;
                case GameConfig.ObstructionModel.HURT_BOUNCE:
                    UnityEngine.Debug.Log("ObstructionModel.HURT_BOUNCE");
                    break;
                case GameConfig.ObstructionModel.ENEMY_BALLON_HURT:
                    if ((user.MonstHP-=1)<=0)
                    {
                        CustomPropsEventData d = new CustomPropsEventData();
                        d.type = GameConfig.PropsState.PROPS_COMMAND;
                        d.propStatus = GameConfig.PropsState.WIN;
                        dispatcher.Dispatch(GameConfig.PropsState.PROPS_COMMAND, d);
                    }
                    data.currentObj.SetActive(false);
                    break;
                default:
                    break;
            }
        }
    }
}
